import Axios from "axios"

const axios = Axios.create({
    baseUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
    headers: {
        "X-Requested-With": "XMLHttpRequest"
    },
    withCredentials: true
})
export default axios