import MainLayout from "../layouts/MainLayout"
import {useState} from "react"
import {useAuth} from '../hooks/auth'
import Label from '../components/Label'
import Input from '../components/Input'
import Button from '../components/Button'
import Head from 'next/head'
import Errors from "../components/Errors"

const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errors, setErrors] = useState([])
    const {login, isLoading, user} = useAuth({middleware: "guest"})
    /*if (isLoading || user) {
        return (
            <>Is Loading</>
        )
    }*/
    const submitForm = async e => {
        e.preventDefault()
        login({email, password, setErrors})
    }
    return (
        <>
            <Head>
                <title>Login</title>
            </Head>
            <div className="mx-auto bg-white shadow p-2 rounded">
                <Errors errors={errors}/>
                <form onSubmit={submitForm} autoComplete="off" className="space-y-4">
                    <div className="">
                        <Label htmlFor="email">Email</Label>
                        <Input
                            id="email"
                            type="email"
                            value={email}
                            className="w-full"
                            onChange={e => setEmail(e.target.value)}
                            required
                            autoFocus
                            autoComplete="off"
                        />
                    </div>
                    <div className="">
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            type="password"
                            value={password}
                            className="w-full"
                            onChange={e => setPassword(e.target.value)}
                            required
                            autoFocus
                            autoComplete="off"
                        />
                    </div>
                    <div className="">
                        <Button>Login</Button>
                    </div>
                </form>
            </div>
        </>
    )
}
export default Login
Login.getLayout = function getLayout(page) {
    return (
        <MainLayout>
            {page}
        </MainLayout>
    )
}