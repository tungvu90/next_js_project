import MainLayout from '../layouts/MainLayout'

const About = () => {
    return (
        <div>
            <h1>About Page</h1>
        </div>
    )
}
export default About
About.getLayout = function getLayout(page) {
    return (
        <MainLayout>
            {page}
        </MainLayout>
    )
}