import MainLayout from '../layouts/MainLayout'
import Button from '../components/Button'
import Input from '../components/Input'
import Label from '../components/Label'
const Home = () => {
    return (
        <div>
            <h1>Home Page</h1>
            <Button className="login">
                Login
            </Button>
            <Input/>
            <Label className="font-bold">Tên tài khoản</Label>
        </div>
    )
}
export default Home
Home.getLayout = function getLayout(page) {
    return (
        <MainLayout>
            {page}
        </MainLayout>
    )
}
