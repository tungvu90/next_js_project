const Footer = () => {
    return (
        <footer className="flex justify-center p-4 text-white bg-indigo-700">
            <h1 className="flex space-x-2 font-semibold tracking-wide">
                Copyright &copy;
                <span>Tiểu Vũ</span>
            </h1>
        </footer>
    )
}
export default Footer