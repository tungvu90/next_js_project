import Link from 'next/link'

const Nav = () => {
    return (
        <nav className="space-x-2">
            <ul>
                <Link href='/'>Home</Link>
                <Link href='/about'>About</Link>
                <Link href='/'>Services</Link>
            </ul>
        </nav>
    )
}
export default Nav